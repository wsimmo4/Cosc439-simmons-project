#!/usr/bin/env python3
import RPi.GPIO as GPIO  # import GPIO
from hx711 import HX711  # import the class HX711

try:
    GPIO.setmode(GPIO.BCM)  # set GPIO pin mode to BCM numbering
    # Create an object hx which represents your real hx711 chip
    # Required input parameters are only 'dout_pin' and 'pd_sck_pin'
    hx = HX711(dout_pin=5, pd_sck_pin=6)
    # measure tare and save the value as offset for current channel
    # and gain selected. That means channel A and gain 128
    err = hx.zero()
    # check if successful
    if err:
        raise ValueError('Tare is unsuccessful.')

    reading = hx.get_raw_data_mean()
    if reading:  # always check if you get correct value or only False
        # now the value is close to 0
        print('Data subtracted by offset but still not converted to units:',
              reading)
    else:
        print('invalid data', reading)
    input('Place one object on the scale and press Enter')
    reading = hx.get_data_mean()
    if reading:
        print('Mean value from HX711 subtracted by offset:', reading)
        known_weight_grams = input(
            'Press Enter')
        try:
            value = reading
        except ValueError:
            print('Expected integer or float and I have got:',
                  known_weight_grams)
        ratio = reading / value  
        hx.set_scale_ratio(ratio)  
        print('Ratio is set.')
    else:
        raise ValueError('Cannot calculate mean value. Try debug mode. Variable reading:', reading)

    print("Place all objects on the scale")
    input('Press Enter to calculate')
    print('Current amount of items on the scale is ', round(hx.get_weight_mean(20)/value))

except (KeyboardInterrupt, SystemExit): # emergency stop, CTRL + C, unused but there just in case
    print('Bye :)')

finally:
    GPIO.cleanup()
