First, ensure that you have a Raspberry Pi 4 Model B, with a clean install of Raspbian OS on a microSD card.
Hook up your Raspberry Pi and ensure that everything is running properly on it.
Install the files in the github to your Raspberry Pi, in this example I have installed them to the downloads folder.
In the Raspberry Pi Terminal, type "cd Downloads/HX711/HX711_Python3", in order to navigate to the proper folder
Type in the terminal "sudo python3 example.py"
Follow the prompts in the program, being:
1. Place on of the object you are counting onto the load cell, and press Enter
2. Press Enter after a second
3. Place all of your objects to count on the scale, and press Enter
The program will output how many items are on the scale
